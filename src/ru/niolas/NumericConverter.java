package ru.niolas;

import java.util.ArrayList;

public class NumericConverter {

    private static final ArrayList<String[]> digitParts;

    static {
        digitParts = new ArrayList<>(3);
        digitParts.clear();
        digitParts.add(new String[]{"один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"});
        digitParts.add(new String[]{"десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"});
        digitParts.add(new String[]{"сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"});
    }

    private static String thousands(int k) {
        if (k == 1) {
            return "тысяча";
        } else if ((k >= 2) && (k <= 4)) {
            return "тысячи";
        } else {
            return "тысяч";
        }
    }

    private static String millions(int k) {
        if (k == 1) {
            return "миллион";
        } else if ((k >= 2) && (k <= 4)) {
            return "миллиона";
        } else {
            return "миллионов";
        }
    }

    private static String billions(int k) {
        if (k == 1) {
            return "миллиард";
        } else if ((k >= 2) && (k <= 4)) {
            return "миллиарда";
        } else {
            return "миллиардов";
        }
    }

    private static String trillions(int k) {
        if (k == 1) {
            return "триллион";
        } else if ((k >= 2) && (k <= 4)) {
            return "триллиона";
        } else {
            return "триллионов";
        }
    }

    private static String one(int exp) {
        if (exp == 3) {
            return "одна";
        } else {
            return "один";
        }
    }

    private static String two(int exp) {
        if (exp == 3) {
            return "две";
        } else {
            return "два";
        }
    }

    public static String toAlpha(long number) throws NotANaturalNumberException, RangeViolationException {
        if (number <= 0) {
            throw new NotANaturalNumberException();
        }

        if (number >= 1000000000000000L) {
            throw new RangeViolationException();
        }

        String numberString = String.valueOf(number);

        StringBuilder builder = new StringBuilder(numberString.length());

        for (int i = 0; i < numberString.length(); i++) {
            int digit = Integer.parseInt(String.valueOf(numberString.charAt(i)));
            int exp = numberString.length() - 1 - i;
            int internalExp = exp % 3;
            int externalExp = exp / 3;
            if (digit != 0) {
                if (internalExp == 0) {
                    if (digit == 1) {
                        builder.append(one(exp));
                        builder.append(" ");
                    } else if (digit == 2) {
                        builder.append(two(exp));
                        builder.append(" ");
                    } else {
                        builder.append(digitParts.get(internalExp)[digit - 1]);
                        builder.append(" ");
                    }
                } else {
                    builder.append(digitParts.get(internalExp)[digit - 1]);
                    builder.append(" ");
                }
            }
            if (internalExp == 0) {
                switch (externalExp) {
                    case 1:
                        builder.append(thousands(digit));
                        builder.append(" ");
                        break;
                    case 2:
                        builder.append(millions(digit));
                        builder.append(" ");
                        break;
                    case 3:
                        builder.append(billions(digit));
                        builder.append(" ");
                        break;
                    case 4:
                        builder.append(trillions(digit));
                        builder.append(" ");
                        break;
                    default:
                        break;
                }
            }
        }

        return builder.toString().trim();
    }

}
